$(document).ready(function () {
    let previousDice1 = ''; // show-item for first dice
    let previousDice2 = ''; // show-item for second dice
    let element1 = $('#dice'); //first dice
    let element2 = $('#dice-2'); // second dice

    $('#generate').on('click', function () {
        let arr = ['one', 'two', 'three', 'four', 'five', 'six'];
        element1.removeClass(previousDice1);    //remove class of previous active item first dice
        element2.removeClass(previousDice2); //remove class of previous active item from second dice
        let rand = randomGenerate(0, 6);    // generating random number from 0 to 6(inclusive)
        element1.addClass(`show-${arr[rand]}`); // activating item
        previousDice1 = `show-${arr[rand]}`;
        rand = randomGenerate(0, 6);
        element2.addClass(`show-${arr[rand]}`);
        previousDice2 = `show-${arr[rand]}`;
    });

    /**
     * 
     * @param {int} min min number which can be generated
     * @param {int} max max number which can be generated
     */
    function randomGenerate(min, max) {
        let rand = Math.random() * (max - min) + min;
        return Math.ceil(rand);
    }
});
